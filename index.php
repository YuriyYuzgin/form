<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/main.css" type="text/css" />
    <title>form</title>
</head>
<body>
<div>
    <form action="server.php" method="POST">
        <h3>Введите информацию о себе.</h3>
        <p>Логин (только из букв и цифр):</p>
        <input type="text" name="user_name" class="errors" value="<?=$_SESSION['user_name']?>">
		<p class="error"><?=$_SESSION['error_user_name']?><?=$_SESSION['user_name_not_available']?><?=$_SESSION['error_symbols']?></p>
        <p>Адрес электронной почты:</p>
        <input type="email" name="email" class="errors" value="<?=$_SESSION['email']?>">
		<p class="error"><?=$_SESSION['error_email']?><?=$_SESSION['email_not_available']?></p>
        <p>Пароль:</p>
        <input type="password" name="pass">
		<p class="error"><?=$_SESSION['error_pass']?></p><br>
        <input type="submit">
    </form>
	<p><?=$_SESSION['error_connect']?></p>
</div>
</body>
</html>
<?
unset($_SESSION['error_connect'], 
	  $_SESSION['error_user_name'], 
	  $_SESSION['error_email'], 
	  $_SESSION['error_pass'], 
	  $_SESSION['error_symbols'], 
	  $_SESSION['user_name_not_available'], 
	  $_SESSION['email_not_available']);

session_destroy();
?>