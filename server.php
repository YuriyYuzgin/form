<?php
session_start();

$user_name = htmlspecialchars(strip_tags($_POST[user_name])); 
$email = htmlspecialchars(strip_tags($_POST[email]));
$pass = htmlspecialchars(strip_tags($_POST[pass]));

require_once("classes/Database.php");
require_once("classes/User.php");

$newUser = new User();
$newUser->form($user_name, $email, $pass);	//Проверка на заполнение полей формы.
$newUser->symbols($user_name);	//Проверка логина на допустимые символы.

// Проверка логина и email на уникальность.
$db = new Database();
$getRow = $db->getRow(
	"SELECT * 
	   FROM `registration` 
	  WHERE user_name=? 
	 	OR email=?", 
	["$user_name","$email"]);
if(($user_name == $getRow[user_name]) && ($_SESSION['user_name']="$user_name"))
			$_SESSION['user_name_not_available']='Данный логин занят!';
		if(($email == $getRow[email]) && ($_SESSION['email']="$email"))
			$_SESSION['email_not_available']='Данный email уже зарегистрирован!';

// Если есть ошибки - переход к форме.
if (!(empty($_SESSION['error_user_name'])) || 
	!(empty($_SESSION['error_email'])) || 
	!(empty($_SESSION['error_pass'])) ||
	!(empty($_SESSION['error_symbols'])) ||
	!(empty($_SESSION['user_name_not_available'])) ||
	!(empty($_SESSION['email_not_available']))){
		header("Location: index.php");
} else {
	//Заносим данные в базу данных.
	$hash = password_hash($pass, PASSWORD_DEFAULT);
	$insertRow = $db->insertRow(
	"INSERT INTO `registration` 
		(`user_name`, `email`, `password`) 
	VALUES 
		(?, ?, ?)", 
	[$user_name, $email, $hash]);
	
	$newUser->info($user_name, $email, $pass);
}
?>