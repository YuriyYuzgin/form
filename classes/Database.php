<?php
class Database{
	public $isConn;
	protected $datab;
	
	// подключение
	public function __construct($host = "localhost", $dbname="form", $username = "root", $password = "", $options = []){
		$this->isConn = TRUE;
		try{
			$this->datab = new PDO("mysql:host={$host};dbname={$dbname};charset=utf8", $username, $password, $options);
		} catch (PDOException $e){
			$_SESSION['error_connect']="Не удалось подключиться к базе данных.<br>Попробуйте ещё раз!";
			header("Location: index.php");
		}
	}
	
	// отсоединение
	public function Disconnect(){
		$this->datab = NULL;
		$this->isConn = FALSE;
	}
	// Выборка строк
	public function getRow($query, $params = []){
		try{
			$stmt = $this->datab->prepare($query);
			$stmt->execute($params);
			return $stmt->fetch();
		} catch (PDOException $e){
			throw new Exception($e->getMessage());
		}
	}
	// Добавление строк
	public function insertRow($query, $params = []){
		try{
			$stmt = $this->datab->prepare($query);
			$stmt->execute($params);
		} catch (PDOException $e){
			throw new Exception($e->getMessage());
		}
	}
}
?>